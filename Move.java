/**
 * Created by lilyhoffman on 4/23/16.
 * Represents a move made in a single turn in connect four (placing a X in a column)
 * 
 * Each move has a perceived value computed by my heuristic
 *
 * Data from the heuristic is stored in "value" 
 * Data about which move is being analyzed is stored in "column"
 */
public class Move {
    public int col;
    public double value;


    public Move(){
        this.col = 0;
        this.value = 0.0;
    }

    public Move(int col, double value){
        this.col = col;
        this.value = value;
    }


    public Move(int col){
        this.col = col;
        this.value = 0.0;
    }


    public double getValue(){
        return this.value;
    }

    public void setValue(double value){
        this.value = value;
    }
}
