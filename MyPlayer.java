import java.util.ArrayList;
import java.util.Random;
import java.util.Collections;

/**
 * University of San Diego
 * COMP 285: Spring 2016
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 *
 *
 * Lily Hoffman:
 * Heuristic: double wins, single wins, odd/even threats, two in a rows, dominating middle
 * Iterative deepening
 * Alpha beta pruning
 */
public class MyPlayer extends Player {

    int moveCount = 0;
    Random rand;


    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    /*
     * Selects a move based on the miniMax algorithm and my heuristc
     */
    public int chooseMove(Board gameBoard) {
        // Start measuring elapsed time
        int opponent = (this.playerNumber == 1)? 2:1;

        long start = System.nanoTime();
        
        //First two moves in column 3
        if (moveCount++  <=1)
            return 3;
        
        //Quick offense
        int quickWin= checkQuickWin(gameBoard, opponent);
        if(quickWin!= -10){
            return quickWin;
        }
        
        //Quick Defense
        int quickBlock = checkQuickWin(gameBoard, this.playerNumber);
        if(quickBlock != -10){
            return quickBlock;
        }

        Move opt = new Move();
        int i = 10;
        long diff = System.nanoTime() - start;
        while(diff < 9e9){
            opt = miniMax(gameBoard, this.playerNumber, i++, -1, 1);
            diff =System.nanoTime() - start;
        }

        double elapsed = (double) diff / 1e9;
        int col = opt.col;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        System.out.println("*******************************************");
        System.out.println("Move: " + col + " Value: " + opt.value);
        System.out.println("*******************************************");

        return col;
    }


    /* Implements the minimax algorithm with alpha-beta using my heuristic
     */
    public Move miniMax(Board b, int playerNum, int depth, double alpha, double beta) {
        int otherPlayer = (playerNum == 1) ? 2 : 1;
        Move best = new Move(0,0.0);

        //Maximizing player
        if (playerNum == 1) {
            best.value = -2.0;
            for (int i = 0; i < 7; i++) {
                if (!b.isColumnOpen(i))
                    continue;

                //Make move
                b.move(playerNum, i);
                int state = b.checkIfGameOver(i);
                Move current = new Move(i, 0.0);

                //Evaluate board
                if (state == 1 || state == 2 || state == 0) {
                    state = (state == 2 ? -1 : state);
                    current.value = state;
                } else if (depth > 0){
                    Move nextMove = miniMax(b, otherPlayer, depth - 1, alpha, beta);
                    current.value = nextMove.value;
                } else {
                    current.value = heuristic(b, playerNum);
                }

                b.undoMove(i);

                //Alpha-beta pruning
                if (current.value > best.value) {
                    best = current;
                }
                alpha = Math.max(alpha, best.value);
                if (beta <= alpha)
                    break;
            }

            return best;

        }
        else {
            best.value = 2.0;

            for (int i = 0; i < 7; i++) {
                if (!b.isColumnOpen(i))
                    continue;

                //Make move
                b.move(playerNum, i);
                int state = b.checkIfGameOver(i);
                Move current = new Move(i, 0.0);

                //Evaluate board
                if (state == 1 || state == 2 || state == 0) {
                    state = (state == 2 ? -1 : state);
                    current.value = state;
                } else if (depth > 0){
                    Move nextMove = miniMax(b, otherPlayer, depth - 1, alpha, beta);
                    current.value = nextMove.value;
                } else {
                    current.value = heuristic(b, playerNum);
                }

                b.undoMove(i);

                if (current.value < best.value) {
                    best = current;
                }

                //Alpha beta pruning
                beta = Math.min(beta, best.value);
                if (beta <= alpha)
                    break;
            }

            return best;

        }

    }


    /*
     * Heuristic for analyzing the board and determining the perceived value of a move
     * In evaluating the board, the following factors were considered:
        • Priority 1: Multiple unblocked three in a rows (double way wins)
            o Worth: 150-200
        • Priority 2: Single unblocked three in a rows (one way win)
            o Worth: 90-100
        • Priority 3: Odd/Even threats (who controls the board)
            o Worth: 70
        • Priority 4: Unblocked two in a rows
            o Worth: 50
        • Priority 5: Dominating the middle
            o Worth: 30
     */
    private double heuristic(Board gameBoard, int playerNum) {
        int opponentNum = (playerNum == 1) ? 2 : 1;

        int p1Threes = countUnblockedThrees(gameBoard, 1);
        int p2Threes = countUnblockedThrees(gameBoard, 2);
        int p1Twos = countVerticalTwos(gameBoard, 1) + countHorizontalTwos(gameBoard, 1);
        int p2Twos = countVerticalTwos(gameBoard, 2) + countHorizontalTwos(gameBoard, 2);
        int dominatesMiddle = dominatesMiddle(gameBoard, this.playerNumber);
        int p1OddThreat = threatOddEven(gameBoard, 1, 1);
        int p1EvenThreat = threatOddEven(gameBoard, 1, 0);
        int p2OddThreat = threatOddEven(gameBoard, 2, 1);
        int p2EvenThreat = threatOddEven(gameBoard, 2, 0);

        double p1score = 0;
        double p2score = 0;

        //Priority 1: Double way wins
        if(playerNum == 1 && p1Threes > 1)
            p1score += 200;
        if(playerNum == 2 && p2Threes > 1)
            p2score +=200;
        if(playerNum == 2 && p1Threes > 1)
            p1score += 150;
        if(playerNum == 1 && p2Threes > 1)
            p2score += 150;

        //Priority 2: One way win
        if(playerNum == 1 && p1Threes == 1)
            p1score += 100;
        if(playerNum == 2 && p2Threes == 1)
            p2score +=100;
        if(playerNum == 2 && p1Threes == 1)
            p1score += 90;
        if(playerNum == 1 && p2Threes == 1)
            p2score += 90;

        //Priority3: Odd/Even Threats
        /*P2 initially controls

         *P1 establishes control with odd threat
         *P1 controls if p1 has odd threat and p2 has even threat

         *P2 establishes control with even threat
         *P2 controls if p1 has no odd threat

         *No one controls if both have odd threats
         */

        //P2 controls if p1 has no odd threat, or if p2 has even threat
        if(p1OddThreat == 0 || (p2EvenThreat > 0 && (p2EvenThreat > p1OddThreat))){
            p2score += 70;
        }
        //P1 controls if P1 has odd threat, two odd threats cancel though
        //P1 controls if p1 has odd threat and p2 has even threat
        if((p1OddThreat > 0 && p1OddThreat > p2OddThreat) || (p1OddThreat == 1 && p2EvenThreat == 1)){
            p1score+= 70;
        }


        //Priority 4: Two in a rows
        if (p1Twos < p2Twos)
            p2score += 50;
        if(p1Twos > p2Twos)
            p1score += 50;

        //Priority 5: Dominating middle
        if(dominatesMiddle == 1)
            p1score += 30;
        if(dominatesMiddle == 2)
            p2score += 30;

        return (p1score - p2score)/1000;
    }

    private int countUnblockedThrees(Board gameBoard, int player){
        return countHorizontalThrees(gameBoard, player) + countVerticalThrees(gameBoard, player) + countDownwardDiagonals(gameBoard, player) + countUpwardDiagonals(gameBoard, player);
    }


    /**
     * Horizontal unblocked three in a rows
     * @param gameBoard
     * @param player
     * @return number of unblocked three-in-a-rows
     */
    private int countHorizontalThrees(Board gameBoard, int player) {
        int[][] b = gameBoard.getBoard();
        int threes = 0;

        int j = 0;
        while (j < 4) {
            //1110
            if (b[0][j] == player && b[0][j + 1] == player && b[0][j + 2] == player && b[0][j + 3] == 0) {
                threes++;
            }
            //0111
            if (b[0][j] == 0 && b[0][j + 1] == player && b[0][j + 2] == player && b[0][j + 3] == player) {
                threes++;
            }
            //1101
            if (b[0][j] == player && b[0][j + 1] == player && b[0][j + 2] == 0 && b[0][j + 3] == player) {
                threes++;
            }
            //1011
            if (b[0][j] == player && b[0][j + 1] == 0 && b[0][j + 2] == player && b[0][j + 3] == player) {
                threes++;
            }
            j++;
        }


        //Scan Rows looking for unblocked threes (0111, 1011, 1101, 1110)
        for (int i = 1; i < 7; i++) {
            j = 0;
            while (j < 4) {
                //1110
                if (b[i][j] == player && b[i][j + 1] == player && b[i][j + 2] == player && b[i][j + 3] == 0 && b[i - 1][j + 3] != 0) {
                    threes++;
                }
                //0111
                if (b[i][j] == 0 && b[i - 1][j] != 0 && b[i][j + 1] == player && b[i][j + 2] == player && b[i][j + 3] == player) {
                    threes++;
                }
                //1101
                if (b[i][j] == player && b[i][j + 1] == player && b[i][j + 2] == 0 && b[i - 1][j + 2] != 0 && b[i][j + 3] == player) {
                    threes++;
                }
                //1011
                if (b[i][j] == player && b[i][j + 1] == 0 && b[i - 1][j + 1] != 0 && b[i][j + 2] == player && b[i][j + 3] == player) {
                    threes++;
                }
                j++;
            }
        }
        return threes;
    }

    /**
     * Returns player num that dominates middle
     * @param gameBoard
     * @param playerNum
     * @return dominating player #
     */
    private int dominatesMiddle(Board gameBoard, int playerNum){
        int[][] b = gameBoard.getBoard();
        int opponentNum = (playerNum == 1)? 2 : 1;
        int player = 0;
        int opponent = 0;


        for(int i = 0; i < 7; i++){
            for(int j = 2; j <= 4; j++){
                if(b[i][j] == playerNum)
                    player++;
                if(b[i][j] == opponentNum)
                    opponent++;
            }
        }

        if(player > opponent)
            return playerNum;
        if(opponent > player)
            return opponentNum;
        else
            return 0;
    }


    /**
     * Vertical accessible three in a rows
     * @param gameBoard
     * @param player
     * @return number of three-in-a-rows (vertical)
     */
    private int countVerticalThrees(Board gameBoard, int player) {
        int[][] b = gameBoard.getBoard();
        int threes = 0;
        for (int j = 0; j < 7; j++) {
            int i = 0;
            while (i <= 3) {
                if (b[i][j] == player && b[i + 1][j] == player && b[i + 2][j] == player && b[i + 3][j] == 0) {
                    threes++;
                }
                i++;
            }
        }
        return threes;
    }

    /**
     * Vertical accessible two in a rows
     * @param gameBoard
     * @param player
     * @return number of two-in-a-rows (vertical)
     */
    private int countVerticalTwos(Board gameBoard, int player) {
        int[][] b = gameBoard.getBoard();
        int twos = 0;

        for (int j = 0; j < 7; j++) {
            int i = 0;
            while (i < 4) {
                if (b[i][j] == player && b[i + 1][j] == player && b[i + 2][j] == 0 && b[i + 3][j] == 0)
                    twos++;
                i++;
            }
        }
        return twos;
    }

    /**
     * Vertical accessible two in a rows
     * @param gameBoard
     * @param player
     * @return number of two-in-a-rows (horizontal)
     */
    private int countHorizontalTwos(Board gameBoard, int player) {
        int[][] b = gameBoard.getBoard();
        int twos = 0;

        for (int i = 0; i < 1; i++) {
            int j = 0;
            while (j < 4) {
                //1100
                if (b[i][j] == player && b[i][j + 1] == player && b[i][j + 2] == 0 && b[i][j + 3] == 0)
                    twos++;
                //0011
                if (b[i][j] == 0 && b[i][j + 1] == 0 && b[i][j + 2] == player && b[i][j + 3] == player)
                    twos++;
                //1010
                if (b[i][j] == player && b[i][j + 1] == 0 && b[i][j + 2] == player && b[i][j + 3] == 0)
                    twos++;
                //0101
                if (b[i][j] == 0 && b[i][j + 1] == player && b[i][j + 2] == 0 && b[i][j + 3] == player)
                    twos++;
                //1001
                if (b[i][j] == player && b[i][j + 1] == 0 && b[i][j + 2] == 0 && b[i][j + 3] == player)
                    twos++;
                //0110
                if (b[i][j] == 0 && b[i][j + 1] == player && b[i][j + 2] == player && b[i][j + 3] == 0)
                    twos++;
                j++;
            }
        }

        for (int i = 1; i < 7; i++) {
            int j = 0;
            while (j < 4) {
                //1100
                if (b[i][j] == player && b[i][j + 1] == player && b[i][j + 2] == 0 && b[i - 1][j] != 0 && b[i][j + 3] == 0 && b[i + i][j + 3] != 0)
                    twos++;
                //0011
                if (b[i][j] == 0 && b[i - 1][j] != 0 && b[i][j + 1] == 0 && b[i - 1][j + 1] != 0 && b[i][j + 2] == player && b[i][j + 3] == player)
                    twos++;
                //1010
                if (b[i][j] == player && b[i][j + 1] == 0 && b[i - 1][j + 1] != 0 && b[i][j + 2] == player && b[i][j + 3] == 0 && b[i - 1][j + 3] != 0)
                    twos++;
                //0101
                if (b[i][j] == 0 && b[i - 1][j] != 0 && b[i][j + 1] == player && b[i][j + 2] == 0 && b[i - 1][j + 2] == 0 && b[i][j + 3] == player)
                    twos++;
                //1001
                if (b[i][j] == player && b[i][j + 1] == 0 && b[i - 1][j + 1] != 0 && b[i][j + 2] == 0 && b[i - 1][j + 2] != 0 && b[i][j + 3] == player)
                    twos++;
                //0110
                if (b[i][j] == 0 && b[i - 1][j] != 0 && b[i][j + 1] == player && b[i][j + 2] == player && b[i][j + 3] == 0 && b[i - 1][j + 3] != 0)
                    twos++;
                j++;
            }
        }

        return twos;
    }


    /**
     * Upward diagonal accessible three in a rows
     * @param gameBoard
     * @param player
     * @return number of upward diagonal three in a rows
     */
    private int countUpwardDiagonals(Board gameBoard, int player) {
        int[][] b = gameBoard.getBoard();
        int threes = 0;

        for(int i = 0; i <=3; i++) {
            for (int j = 0; j <= 3; j++) {
                if (b[i][j] == player && b[i + 1][j + 1] == player && b[i + 2][j + 2] == player && b[i + 3][j + 3] == 0 && b[i+2][j+3] != 0)
                    threes++;
                if (b[i][j] == player && b[i + 1][j + 1] == player && b[i + 2][j + 2] == 0 && b[i+1][j+2] !=0 && b[i + 3][j + 3] == player)
                    threes++;
                if (b[i][j] == player && b[i + 1][j + 1] == 0 && b[i][j+1]!=0 && b[i + 2][j + 2] == player && b[i + 3][j + 3] == player)
                    threes++;
                if (i> 0 && b[i][j] == 0 && b[i-1][j]!= 0 && b[i + 1][j + 1] == player && b[i + 2][j + 2] == player && b[i + 3][j + 3] == player)
                    threes++;
                if (i==0 && b[i][j] == 0 && b[i + 1][j + 1] == player && b[i + 2][j + 2] == player && b[i + 3][j + 3] == player)
                    threes++;
            }
        }

        return threes;
    }


    /**
     * Downward diagonal accessible three in a rows
     * @param gameBoard
     * @param player
     * @return number of downward diagonal three in a rows
     */
    private int countDownwardDiagonals(Board gameBoard, int player){
        int[][] b = gameBoard.getBoard();
        int threes = 0;
        for(int i = 6; i>=3; i--){
            for(int j = 0; j<=3;j++){
                if (b[i][j] == player && b[i-1][j + 1] == player && b[i-2][j + 2] == player && b[i-3][j + 3] == 0 && i>=4 && b[i-4][j+3] != 0)
                    threes++;
                if (b[i][j] == player && b[i-1][j + 1] == player && b[i-2][j + 2] == player && b[i-3][j + 3] == 0 && i ==3)
                    threes++;
                if (b[i][j] == player && b[i-1][j + 1] == player && b[i-2][j + 2] == 0 && b[i-3][j+2] != 0 && b[i-3][j + 3] == player)
                    threes++;
                if (b[i][j] == player && b[i-1][j + 1] == 0 && b[i-2][j+1] != 0 && b[i-2][j + 2] == player && b[i-3][j + 3] == player)
                    threes++;
                if (b[i][j] == 0 && b[i-1][j] != 0 && b[i-1][j + 1] == player && b[i-2][j + 2] == player && b[i-3][j + 3] == player)
                    threes++;
            }
        }
        return threes;
    }


    /**
     * Threat - 3 in a row, fourth available but not necessarily accessible
     *
     * @param gameBoard
     * @param player
     * @param mod - mod = 0 for even threat, mod = 1 for odd threat
     * @return number of "threats"
     */
    private int threatOddEven(Board gameBoard, int player, int mod){
        int opponent = (player == 1)? 2 : 1;
        int threat = 0;
        int[][] b =  gameBoard.getBoard();

        //Vertical threat
        for (int j = 0; j < 7; j++) {
            int i = 0;
            while (i <= 3) {
                if (b[i][j] == player && b[i + 1][j] == player && b[i + 2][j] == player && b[i + 3][j] == 0 && ((i+3) % 2 == mod))
                    threat++;
                i++;
            }
        }

        for (int i = 1; i < 7; i++) {
            int j = 0;
            while (j < 4) {
                //1110
                if(i%2 == mod){
                    if (b[i][j] == player && b[i][j + 1] == player && b[i][j + 2] == player && b[i][j + 3] == 0) {
                        threat++;
                    }
                    //0111
                    if (b[i][j] == 0 && b[i][j + 1] == player && b[i][j + 2] == player && b[i][j + 3] == player) {
                        threat++;
                    }
                    //1101
                    if (b[i][j] == player && b[i][j + 1] == player && b[i][j + 2] == 0 && b[i - 1][j + 2] != 0 && b[i][j + 3] == player) {
                        threat++;
                    }
                    //1011
                    if (b[i][j] == player && b[i][j + 1] == 0 && b[i][j + 2] == player && b[i][j + 3] == player) {
                        threat++;
                    }
                }
                j++;
            }
        }


        //Upward diagonal threat
        for(int i = 0; i <=3; i++) {
            for (int j = 0; j <= 3; j++) {
                if (b[i][j] == player && b[i + 1][j + 1] == player && b[i + 2][j + 2] == player && b[i + 3][j + 3] == 0 && ((i+3)%2 == mod))
                    threat++;
                if (b[i][j] == player && b[i + 1][j + 1] == player && b[i + 2][j + 2] == 0 && ((i+2)%2 == mod) && b[i + 3][j + 3] == player)
                    threat++;
                if (b[i][j] == player && b[i + 1][j + 1] == 0 && ((i+1)%2 == mod) && b[i + 2][j + 2] == player && b[i + 3][j + 3] == player)
                    threat++;
                if (b[i][j] == 0 && (i%2 == mod)&& b[i + 1][j + 1] == player && b[i + 2][j + 2] == player && b[i + 3][j + 3] == player)
                    threat++;
            }
        }

        //Downward diagonal threat
        for(int i = 6; i>=3; i--){
            for(int j = 0; j<=3;j++){
                if (b[i][j] == player && b[i-1][j + 1] == player && b[i-2][j + 2] == player && b[i-3][j + 3] == 0 && ((i-3)%2 == mod))
                    threat++;
                if (b[i][j] == player && b[i-1][j + 1] == player && b[i-2][j + 2] == 0 && ((i-2)%2 == mod) && b[i-3][j + 3] == player)
                    threat++;
                if (b[i][j] == player && b[i-1][j + 1] == 0 && ((i-1)%2 == mod) && b[i-2][j + 2] == player && b[i-3][j + 3] == player)
                    threat++;
                if (b[i][j] == 0 && (i%2 == mod) && b[i-1][j + 1] == player && b[i-2][j + 2] == player && b[i-3][j + 3] == player)
                    threat++;
            }
        }
        return threat;
    }



    /*
     * Checks the board for an unblocked three-in-a-row
     * @return - column to place move to block the win
     */
    private int checkQuickWin(Board gameBoard, int player) {
        int opponent = (player == 1) ? 2 : 1;
        int[][] b = gameBoard.getBoard();
        //Check verticals for cheap win, block
        for (int j = 0; j < 7; j++) {
            int i = 0;
            while (i <= 3) {
                if (b[i][j] == opponent && b[i + 1][j] == opponent && b[i + 2][j] == opponent && b[i + 3][j] == 0) {
                    return j;
                }
                i++;
            }
        }
        int j = 0;
        while (j < 4) {
            //1110
            if (b[0][j] == opponent && b[0][j + 1] == opponent && b[0][j + 2] == opponent && b[0][j + 3] == 0)
                return (j + 3);
            //0111
            if (b[0][j] == 0 && b[0][j + 1] == opponent && b[0][j + 2] == opponent && b[0][j + 3] == opponent)
                return j;
            //1101
            if (b[0][j] == opponent && b[0][j + 1] == opponent && b[0][j + 2] == 0 && b[0][j + 3] == opponent)
                return (j + 2);
            //1011
            if (b[0][j] == opponent && b[0][j + 1] == 0 && b[0][j + 2] == opponent && b[0][j + 3] == opponent)
                return (j + 1);
            j++;
        }
        //Scan Rows looking for unblocked threes (0111, 1011, 1101, 1110)
        for (int i = 1; i < 7; i++) {
            j = 0;
            while (j < 4) {
                //1110
                if (b[i][j] == opponent && b[i][j + 1] == opponent && b[i][j + 2] == opponent && b[i][j + 3] == 0 && b[i - 1][j + 3] != 0)
                    return (j + 3);
                //0111
                if (b[i][j] == 0 && b[i - 1][j] != 0 && b[i][j + 1] == opponent && b[i][j + 2] == opponent && b[i][j + 3] == opponent)
                    return j;
                //1101
                if (b[i][j] == opponent && b[i][j + 1] == opponent && b[i][j + 2] == 0 && b[i - 1][j + 2] != 0 && b[i][j + 3] == opponent)
                    return (j + 2);
                //1011
                if (b[i][j] == opponent && b[i][j + 1] == 0 && b[i - 1][j + 1] != 0 && b[i][j + 2] == opponent && b[i][j + 3] == opponent)
                    return (j + 1);
                j++;
            }
        }
        //Check upward diagonals for cheap win, block
        for (int i = 0; i <= 3; i++) {
            for (j = 0; j <= 3; j++) {
                if (b[i][j] == opponent && b[i + 1][j + 1] == opponent && b[i + 2][j + 2] == opponent && b[i + 3][j + 3] == 0 && b[i + 2][j + 3] != 0)
                    return (j + 3);
                if (b[i][j] == opponent && b[i + 1][j + 1] == opponent && b[i + 2][j + 2] == 0 && b[i + 1][j + 2] != 0 && b[i + 3][j + 3] == opponent)
                    return (j + 2);
                if (b[i][j] == opponent && b[i + 1][j + 1] == 0 && b[i][j + 1] != 0 && b[i + 2][j + 2] == opponent && b[i + 3][j + 3] == opponent)
                    return (j + 1);
                if (i > 0 && b[i][j] == 0 && b[i - 1][j] != 0 && b[i + 1][j + 1] == opponent && b[i + 2][j + 2] == player && b[i + 3][j + 3] == opponent)
                    return (j);
                if (i == 0 && b[i][j] == 0 && b[i + 1][j + 1] == opponent && b[i + 2][j + 2] == opponent && b[i + 3][j + 3] == opponent)
                    return j;
            }
        }
        //Check downward diagonals for cheap win, block
        for (int i = 6; i >= 3; i--) {
            for (j = 0; j <= 3; j++) {
                if (b[i][j] == opponent && b[i - 1][j + 1] == opponent && b[i - 2][j + 2] == opponent && b[i - 3][j + 3] == 0 && i >= 4 && b[i - 4][j + 3] != 0)
                    return (j + 3);
                if (b[i][j] == opponent && b[i - 1][j + 1] == opponent && b[i - 2][j + 2] == opponent && b[i - 3][j + 3] == 0 && i == 3)
                    return (j + 3);
                if (b[i][j] == opponent && b[i - 1][j + 1] == opponent && b[i - 2][j + 2] == 0 && b[i - 3][j + 2] != 0 && b[i - 3][j + 3] == opponent)
                    return (j + 2);
                if (b[i][j] == opponent && b[i - 1][j + 1] == 0 && b[i - 2][j + 1] != 0 && b[i - 2][j + 2] == opponent && b[i - 3][j + 3] == opponent)
                    return (j + 1);
                if (b[i][j] == 0 && b[i - 1][j] != 0 && b[i - 1][j + 1] == opponent && b[i - 2][j + 2] == opponent && b[i - 3][j + 3] == opponent)
                    return j;
            }
        }
        return -10; //error flag
    }
}
