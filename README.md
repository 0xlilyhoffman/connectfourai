# Connect Four AI
Video demo: https://youtu.be/xAl798D_TuI

##Overview:
I implemented an AI to play connect four based on the Minimax algorithm. The AI can play against a human player, a random player, and an AI implemented based on the the Negamax algorithm.
Connect Four Game:
Connect four is a two player game in which players take turns dropping disks into a 6x7 (or 7x7 for our implementation) grid with the goal of connecting four pieces in a row (vertically, horizontally or diagonally). The game is a zero-sum game, meaning an advantage for one player is equivalent to a disadvantage for the opponent. The game is “strongly solved,” meaning the final outcome of the game can be predicted from any position, assuming each player plays optimally. 

For a game that is “strongly solved,” it is possible to construct an algorithm that determines moves of “optimal play” for each player. An optimal algorithm would search the entire set of possible moves and determine which chain of moves results in win/loss/tie for each player. Game trees can be used to model the evolution of a game and determine possible outcomes for each player. A game tree is a directed graph in which nodes represent the state of the game and edges represent possible moves from a particular state. Given a complete game tree, a win/loss/tie can be foreseen. Practically, this level of evaluation is impossible for many games if the branching factor of possible moves at each stage is large enough to produce an unrealistic number of paths to store and analyze. For these games, a partial game tree is constructed which evaluates future moves up to a limited depth. The search is often cutoff after a certain fixed number of “future moves” have been analyzed. At the cutoff point, the state of the board is analyzed and a heuristic value is given for the perceived worth of the board for each player. 

The use of miniMax trees in game theory utilizes the notion of a “minimizing player” and a “maximizing player” and requires the definition of “minimum score” and a “maximum score.” Each potential state of the game is evaluated and assigned a number. A game state rated at the minimum number represents a win for the minimizing player, while a game state rated at the maximum number represents a win for the maximizing player. All values in between represent the perceived advantage for each player based on the current state of the board, with higher values representing an advantage for the maximizing player and lower values representing an advantage for the minimizing player. If a complete game tree cannot be evaluated, the values assigned to each state are given by a heuristic function, which simply evaluates the state of the board and assigns values based on the perceived advantage for each player. At each step, the maximizing player evaluates all possible moves and picks the move with the highest value. Conversely, the minimizing player evaluates all possible moves and picks the move with the lowest value. The minimax algorithm involves the implicit construction and analysis of the game tree through recursive calls evaluated to a certain specified depth for each potential move – that is, the tree is not explicitly stored and read from to determine moves but rather dynamically created and interpreted.



##AI discussion:

###Minimax tree 
The AI is implemented using the minimax algorithm. The game tree is dynamically created through recursive calls inside a for-loop and is evaluated to determine optimal play for each player. Optimizations of this algorithm have been made through alpha beta pruning and iterative deepening. 

###Alpha beta pruning 
It is not always necessary to evaluate the entire set of potential moves when determining which move to make. Due to the directly opposing interests of the minimizing and maximizing players, certain assumptions can be made about when further optimizations cannot be made and when certain moves do not need to be evaluated. The implementation of alpha beta pruning requires tracking the maximum value that the maximizing player can score (alpha) and the minimum value that the minimum player can score (beta). If a node is evaluated and beta is less than alpha, the children of this node do not need to be searched any further, as no optimizations can be made. The resulting benefit of alpha beta pruning is that less of the tree is searched. 

###Iterative deepening 
With iterative deepening, the first search is performed on a very shallow depth and the optimal move for that depth is stored. This depth is then incremented until the total search time nears the cutoff time allowed per each player in determining a move. This ensures that each potential move has been evaluated to some depth and that at least a “decent” move is stored at all times (better moves are found from searching deeper in the tree). Iterative deepening was implemented due to the combination of two factors: (1) each player must make a move in under 10 seconds, (2) the miniMax algorithm searches a “tree” using recursive calls inside a for loop. If the miniMax function must cut off before maximum specified depth is reached, only a fraction of the moves may have been evaluated. Iterative deepening is implemented by placing the call to miniMax inside a while loop, calling it each time with i++ until elapsed time is 9 seconds. The price paid for iterative deepening is searching some nodes multiple times. 

###Heuristic 
The heuristic evaluates the board at any given point and determines whether the set up is advantageous for player1 or player2. The players accumulate points for advantageous configurations. The amount of points awarded is proportional to the value of the particular configuration. The heuristic also considered whose turn it was, and awarded more points if the player with the advantage was currently up to make a move. At the end of the heuristic method, player2’s score is subtracted form player1’s score and the result is divided by 1000. This ensures that a positive number indicates an advantage for player1 while a negative number indicates an advantage for player2. The number is divided by 1000 to ensure the value falls between -1 and 1.
In evaluating the board, the following factors were considered: 

* Priority 1: Multiple unblocked three in a rows (double way wins)
* Priority 2: Single unblocked three in a rows (one way win) 
* Priority 3: Odd/Even threats (who controls the board)
* Priority 4: Unblocked two in a rows
* Priority 5: Dominating the middle